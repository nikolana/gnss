function [loopDLL, loopPLL, loopFLL] = discriminators(settings, loopDLL, loopPLL, loopFLL, k, index)
    %%              Costas loop discriminators
    %
    % PLL discriminators can be found in Kaplan & Hegarty - table 8.20
    % FLL discriminators can be found in Kaplan & Hegarty - table 8.21
    % DLL discriminators can be found in Kaplan & Hegarty - table 8.22
    %
    % Input:
    %
    %   settings   - user defined settings (sampling freq., doppler freq.,
    %                phase shift, ...)
    %   loopDLL    - Delay locked loop structure
    %   loopPLL    - Phase locked loop strucrure
    %   loopFLL    - Frequency locked loop structure
    %   k          - k-th iteration of the loop
    %   index      - index of the LHS vector (to avoid zero padding in plots) 
    %
    % Output:
    %
    %   loopDLL    - Delay locked loop structure
    %   loopPLL    - Phase locked loop strucrure
    %   loopFLL    - Frequency locked loop structure
    %
    %%=========================================================================
    %% DLL discriminator
    if strcmp(settings.dsc.DLL, 'e-l') 
        loopDLL.dsc_out(index) = 1/2*(sqrt(loopDLL.Ie(k)^2+loopDLL.Qe(k)^2)-sqrt(loopDLL.Il(k)^2+loopDLL.Ql(k)^2))/...
                                 (sqrt(loopDLL.Ie(k)^2+loopDLL.Qe(k)^2)+sqrt(loopDLL.Il(k)^2+loopDLL.Ql(k)^2));
    elseif strcmp(settings.dsc.DLL, 'e-l_pow')
        loopDLL.dsc_out(index) = 1/2*(sqrt(loopDLL.Ie(k)^2+loopDLL.Qe(k)^2)-sqrt(loopDLL.Il(k)^2+loopDLL.Ql(k)^2));
    elseif strcmp(settings.dsc.DLL, 'quasi')
        loopDLL.dsc_out(index) = 1/4*((loopDLL.Ie(k)-loopDLL.Il(k))/loopDLL.Ip(k)+...
                                  (loopDLL.Qe(k)-loopDLL.Ql(k))/loopDLL.Qp(k));
    elseif strcmp(settings.dsc.DLL, 'dot')
        loopDLL.dsc_out(index) = 1/4*((loopDLL.Ie(k)-loopDLL.Il(k))/loopDLL.Ip(k));
    else
        error('DLL discriminator not supported!')
    end

    %% FLL discriminator
    loopFLL.dot(index)     = loopPLL.I_ps1*loopPLL.I_ps2+loopPLL.Q_ps1*loopPLL.Q_ps2;
    loopFLL.cross(index)   = loopPLL.I_ps1*loopPLL.Q_ps2-loopPLL.I_ps2*loopPLL.Q_ps1;
    if strcmp(settings.dsc.FLL, 'cross')
        loopFLL.dsc_out(index) = loopFLL.cross(k)/(2*pi*loopPLL.PDI); 
    elseif strcmp(settings.dsc.FLL, 'sign')
        loopFLL.dsc_out(index) = loopFLL.cross(k)*sign(loopFLL.dot(k))/(2*pi*loopPLL.PDI);
    elseif strcmp(settings.dsc.FLL, 'atan2') 
        loopFLL.dsc_out(index) = atan2(loopFLL.cross(k),loopFLL.dot(k))/(2*pi*loopPLL.PDI);
    elseif strcmp(settings.dsc.FLL, 'atan')
        accum_len = loopPLL.PDI*1e3;
        if loopFLL.dot(index) == 0
            loopFLL.dsc_out(index) = 0;
        else
            loopFLL.dsc_out(index) = atan(loopFLL.cross(k)/loopFLL.dot(k))/(2*pi*accum_len*1e-3);
        end
    else
        error('FLL discriminator not supported!')
    end

    %% PLL discriminator
    if strcmp(settings.dsc.PLL, 'Q*I')
        loopPLL.dsc_out(index) = loopDLL.Qp(k)*loopDLL.Ip(k);
    elseif strcmp(settings.dsc.PLL, 'sign')
        loopPLL.dsc_out(index) = loopDLL.Qp(k)*sign(loopDLL.Ip(k))
    elseif strcmp(settings.dsc.PLL, 'Q/I')
        loopPLL.dsc_out(index) = loopDLL.Qp(k)/loopDLL.Ip(k);
    elseif strcmp(settings.dsc.PLL, 'atan')
        loopPLL.dsc_out(index) = atan(loopDLL.Qp(k)/loopDLL.Ip(k));
    elseif strcmp(settings.dsc.PLL, 'atan2')
        loopPLL.dsc_out(index) = atan2(loopDLL.Qp(k), loopDLL.Ip(k));            
    else
        error('PLL discriminator not supported!')
    end
end