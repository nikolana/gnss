function [resultsTCK] = tracking(settings, resultsGEN, resultsACQ)
    %%              Code and carrier tracking of GNSS signal
    % Input:
    %
    %   settings   - user defined settings (sampling freq., doppler freq.,
    %                phase shift, ...)
    %   resultsGEN - results of signal generation
    %   resultsACQ - results of signal acquisition 
    %
    % Output:
    %
    %   resultsTCK - results of code and carrier tracking
    %
    %%=========================================================================
                          %%%% Local variabels %%%%                          
    sig_len    = settings.sig_len;
    if strcmp(settings.constellation, 'galileo')
        accum_len = 4;
        ms        = 4e-3;
        prn       = resultsGEN.prn;
        code_len  = length(prn);
        prn       = [ prn(end) prn prn(1) ];
    elseif strcmp(settings.constellation, 'gps L5')
        prn       = resultsGEN.prn;
        code_len  = length(prn);
        prn       = [ prn(end) prn prn(1) ];
        accum_len = 1;
        ms        = 1e-3;
    elseif strcmp(settings.constellation, 'gps L1')
        prn       = resultsGEN.prn;
        code_len  = length(prn);
        prn       = [ prn(end) prn prn(1) ];
        accum_len = 1;
        ms        = 1e-3;
    else error('Constellation not supported, aborting!')
    end
    
    % Initialize loop structures
    [loopDLL, loopPLL, loopFLL, codeNCO] = initTracking(settings, resultsACQ);
    
    % Rx-Tx velocity 
    v_dll      = zeros(1, settings.sig_len);
    v_pll      = zeros(1, settings.sig_len);
    c          = 299792458; % speed of light [m/s]
    
    % indexing 
    m = settings.acq_len*accum_len;
    ptr = settings.acq_len*settings.f_sampling*ms-loopDLL.P; 
    
    % signal related
    signal     = resultsGEN.signal;
    phase_est  = 0;
    phase_rem  = 0; 
    f_est      = -resultsACQ.fd_noncoh; % estimate carrier frequency
    f_basis    = -resultsACQ.fd_noncoh; 
    
    w = waitbar(0, 'Tracking...');
    pause(1)
    
    %%=========================================================================
                         %%%% Main Loop %%%%
    for k = (m+accum_len):accum_len:sig_len
        pct = (k-settings.acq_len)*(90/(sig_len-settings.acq_len))/100;
        waitbar(pct, w, ['Processing block [ ', num2str(k), '/', num2str(sig_len), ' ]'])
        if (k+accum_len-1) <= sig_len
            index = k:(k+accum_len-1); % if accum_len > 1 -> copy last value until 
        else index = k:sig_len;         % accumulation is finished (prevents zero "padding" 
        end                            % the plots) 
        %% Update code NCO
        codeNCO.freq = codeNCO.fc - loopDLL.filt_out(k-accum_len) * settings.DLL_fb; % [Hz]
        
        % use the line below for carrier-assisted DLL (experimental)
        %codeNCO.freq = codeNCO.fc - loopDLL.filt_out(k-accum_len) * settings.DLL_fb + 1/1540*loopPLL.filt_out(k-accum_len);
        
        delta_phase = codeNCO.freq / codeNCO.fs;
        samples = ceil((code_len - phase_rem) / delta_phase); 
        ptr2 = ptr+samples-1;

        if ptr2 > (resultsACQ.samples_period*sig_len/accum_len)
            disp('Reached the end of the signal!')
            disp(['Last sample read = ', num2str(ptr-1)])
            break;
        end
        
        
        % replica early
        t_chip       = (phase_rem-settings.corr_wdth) : delta_phase : ...
            ((samples-1)*delta_phase+phase_rem-settings.corr_wdth);
        t_samp      = ceil(t_chip) + 1;
        replicaE   = prn(t_samp);

        % replica late
        t_chip       = (phase_rem+settings.corr_wdth) : delta_phase : ...
            ((samples-1)*delta_phase+phase_rem+settings.corr_wdth);
        t_samp      = ceil(t_chip) + 1;
        replicaL    = prn(t_samp);

        % replica prompt
        t_chip       = phase_rem : ...
            delta_phase : ...
            ((samples-1)*delta_phase+phase_rem);
        t_samp      = ceil(t_chip) + 1;
        replicaP  = prn(t_samp);

        phase_rem = (t_chip(samples) + delta_phase) - code_len;
        
        %% Update carrier NCO
        if settings.carrier_on
            t = (0:(samples)) ./ codeNCO.fs;
            carrNCO.arg = 2*pi*f_est.*t + phase_est;
            ex = cos(carrNCO.arg(1:samples)) - 1j*sin(carrNCO.arg(1:samples));
            phase_est = rem(carrNCO.arg(samples+1), 2*pi); % ensures continous phase
        else ex = ones(1, samples);
        end
                
        % estimate mutual velocity
        v_dll(index) = c*(codeNCO.freq-codeNCO.fc)/settings.f_chipn * settings.DLL_fb;
        v_pll(index) = -c*(f_est)/settings.f_carrier * (settings.PLL_fb+settings.FLL_fb);
        
        % read-in ptr2-ptr samples of Rx signal
        sig = signal(ptr:ptr2).*ex;  
        
        % point at the sample to read in next iteration 
        ptr = ptr2+1;
        
        %% Correlators
        % Prompt
        loopDLL.epl(index,2) = sum(sig.*replicaP);
        loopDLL.Ip(index)    = real(loopDLL.epl(index,2));
        loopDLL.Qp(index)    = imag(loopDLL.epl(index,2));
    
        % Early     
        loopDLL.epl(index,1) = sum(sig.*replicaE); 
        loopDLL.Ie(index)    = real(loopDLL.epl(index,1));
        loopDLL.Qe(index)    = imag(loopDLL.epl(index,1));
    
        % Late           
        loopDLL.epl(index,3) = sum(sig.*replicaL);
        loopDLL.Il(index)    = real(loopDLL.epl(index,3));
        loopDLL.Ql(index)    = imag(loopDLL.epl(index,3));
    
        loopPLL.I_ps1 = loopPLL.I_ps1 + loopDLL.Ip(k);
        loopPLL.Q_ps1 = loopPLL.Q_ps1 + loopDLL.Qp(k);
        
        %% Filters
        [loopDLL, loopPLL, loopFLL] = discriminators(settings, loopDLL, loopPLL, loopFLL, k, index);
        [loopDLL, loopPLL, loopFLL] = filters(settings, loopDLL, loopPLL, loopFLL, k, accum_len, index);    

        if settings.PLL_fb
            f_est = f_basis + loopPLL.filt_out(k);
        elseif settings.FLL_fb
            f_est = f_basis - loopFLL.filt_out(k);
        end

        loopPLL.I_ps2 = loopPLL.I_ps1;
        loopPLL.Q_ps2 = loopPLL.Q_ps1;

        loopPLL.I_ps1 = 0;
        loopPLL.Q_ps1 = 0;

        codeNCO.freq = codeNCO.fc;
    end
    resultsTCK.replicaP = replicaP;    
    resultsTCK.DLL   = loopDLL;
    resultsTCK.PLL   = loopPLL;
    resultsTCK.FLL   = loopFLL;
    waitbar(1, w)
    pause(1)
    close(w)
    %%=========================================================================
    %% DLL plot
    t = 1:sig_len;
    f = figure('visible', settings.plot.DLL);
    subplot(1,1,1)
    plot(loopDLL.dsc_out, '-.', 'LineWidth', 3);
    title('DLL discriminator - output')
    xlabel('time [ms]')
    ylim([-1 1])
    grid on
    axis square
    saveas(f, '../figures/DLL.fig');

    f = figure('visible', settings.plot.filt);
    plot(t, loopDLL.acc, t, loopDLL.acc2, 'LineWidth', 3)
    hold on
    plot(t, loopDLL.filt_out, '-.');
    title('DLL filter response')
    xlabel('time [ms]')
    legend('accumulator', 'accumulator2', 'filter output')
    grid on
    axis square
    saveas(f, '../figures/filtDLL.fig')
    
    f = figure('visible', settings.plot.veloc);
    plot(t, v_pll, 'LineWidth', 3);
    hold on
    plot(t, resultsGEN.RxTx_veloc(1:length(t)), 'LineWidth', 3);
    axis square
    grid on
    xlabel('time [ms]');
    ylabel('velocity [m/s]');
    legend('Estimated', 'Actual');
    saveas(f, '../figures/est_velocity.fig');           
    
    f = figure('visible', settings.plot.cor);
    subplot(1, 2, 1)
    plot(t, loopDLL.Ie, '-.', 'LineWidth', 3);
    hold on
    plot(t, loopDLL.Ip, '-.', 'LineWidth', 3);
    plot(t, loopDLL.Il, '-.', 'LineWidth', 3);
    title('Early, Prompt, Late - In-phase')
    legend('Early', 'Prompt', 'Late')
    xlabel('time [ms]')
    grid on
    axis square
    
    subplot(1, 2, 2)
    plot(t, loopDLL.Qe, '-.', 'LineWidth', 3);
    hold on
    plot(t, loopDLL.Qp, '-.', 'LineWidth', 3);
    plot(t, loopDLL.Ql, '-.', 'LineWidth', 3);
    title('Early, Prompt, Late - Quadrature')
    legend('Early', 'Prompt', 'Late')
    xlabel('time [ms]')
    grid on
    axis square
    saveas(f, '../figures/correlators.fig');
    
    f = figure('visible', settings.plot.PLL);
    plot(t, loopPLL.dsc_out*180/pi, '-.', 'LineWidth', 3);
    title('PLL discriminator - output')
    xlabel('time [ms]')
    ylabel('estimated error [deg]')
    ylim([-100 100])
    grid on
    axis square
    saveas(f, '../figures/PLL.fig')

    f = figure('visible', settings.plot.filt);
    plot(t, loopPLL.acc, t, loopPLL.acc2, 'LineWidth', 3)
    hold on
    plot(t, loopPLL.filt_out, '-.');
    title('PLL filter response')
    xlabel('time [ms]')
    legend('accumulator', 'accumulator2', 'filter output')
    grid on
    axis square
    saveas(f, '../figures/filtPLL.fig')

    f = figure('visible', settings.plot.FLL);
    plot(t, loopFLL.dsc_out, '-.', 'LineWidth', 3);
    title('FLL discriminator - output')
    xlabel('time [ms]')
    ylabel('estimated error [Hz]')
    grid on
    axis square
    saveas(f, '../figures/FLL.fig')

    f = figure('visible', settings.plot.filt);
    plot(t, loopFLL.acc, t, loopFLL.acc2,t ,loopFLL.filt_out, '-.', 'LineWidth', 3);
    title('FLL filter response')
    xlabel('time [ms]')
    legend('accumulator', 'accumulator2', 'filter output')
    grid on
    axis square
    saveas(f, '../figures/filtFLL.fig')
end
    