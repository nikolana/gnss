function [loopDLL, loopPLL, loopFLL] = filters(settings, loopDLL, loopPLL, loopFLL, k, n, index)
    %%              Loop filters
    % For loop filters characteristics see Kaplan & Hegarty - table 8.23
    % 
    % Filter equations were derived from systems in K&H - figure 8.52
    % with either boxcar or bilinear integrators (see figure 8.53)
    %                                                    
    % Input:
    %
    %   settings   - user defined settings (sampling freq., doppler freq.,
    %                phase shift, ...)
    %   loopDLL    - Delay locked loop structure
    %   loopPLL    - Phase locked loop strucrure
    %   loopFLL    - Frequency locked loop structure
    %   k          - k-th iteration of the loop
    %   n          - accumulation length
    %   index      - index of the LHS vector (to avoid zero padding in plots) 
    %
    % Output:
    %
    %   loopDLL    - Delay locked loop structure
    %   loopPLL    - Phase locked loop strucrure
    %
    %%========================================================================= 
    %% PLL Filter
    if settings.filt_ord.PLL == 1 
        om_0 = settings.st_ord.PLL_om0;
        loopPLL.filt_out(index) = om_0*loopPLL.dsc_out(k);
    elseif settings.filt_ord.PLL == 2
        om_0 = settings.nd_ord.PLL_om0;
        a = settings.nd_ord.PLL_a;  
        loopPLL.acc(index)      = loopPLL.PDI*(om_0)^2*loopPLL.dsc_out(k) + loopPLL.acc(k-n);
        loopPLL.filt_out(index) = om_0*a*loopPLL.dsc_out(k) + 0.5*(loopPLL.acc(k) + loopPLL.acc(k-n));
    elseif settings.filt_ord.PLL == 3
        om_0 = settings.rd_ord.PLL_om0;
        a = settings.rd_ord.PLL_a;
        b = settings.rd_ord.PLL_b;
        loopPLL.acc(index) = loopPLL.PDI*om_0^3*loopPLL.dsc_out(k)+...
                             loopPLL.acc(k-n);
        loopPLL.acc2(index)= loopPLL.PDI*(0.5*(loopPLL.acc(k)+...
                             loopPLL.acc(k-n))+a*om_0^2*loopPLL.dsc_out(k))...
                            +loopPLL.acc2(k);
        loopPLL.filt_out(index) = 0.5*(loopPLL.acc2(k)+loopPLL.acc2(k-n))...
                                  +b*om_0*loopPLL.dsc_out(k); 
    end

    %% FLL Filter
    if settings.filt_ord.FLL == 1 
        om_0 = settings.st_ord.FLL_om0;
        loopFLL.acc(index) = loopPLL.PDI*om_0*loopFLL.dsc_out(k)+loopFLL.acc(k-n);
        loopFLL.filt_out(index) = 0.5*(loopFLL.acc(k)+loopFLL.acc(k-n));
    elseif settings.filt_ord.FLL == 2
        om_0 = settings.nd_ord.FLL_om0;
        a = settings.nd_ord.FLL_a;
        loopFLL.acc(index) = loopPLL.PDI*(om_0)^2*loopFLL.dsc_out(k)+loopFLL.acc(k-n);
        loopFLL.acc2(index) = loopPLL.PDI*(0.5*(loopFLL.acc(k)+loopFLL.acc(k-n))+...
                          a*om_0*loopFLL.dsc_out(k))+loopFLL.acc2(k-n);
        loopFLL.filt_out(index) = 0.5*(loopFLL.acc2(k)+loopFLL.acc2(k-n));
    elseif settings.filt_ord.FLL == 3
        om_0 = settings.rd_ord.FLL_om0;
        a = settings.rd_ord.FLL_a;
        b = settings.rd_ord.FLL_b;
        loopFLL.acc(index) = loopPLL.PDI*om_0^3*loopFLL.dsc_out(k)+...
                             loopFLL.acc(k-n);
        loopFLL.acc2(index)= loopPLL.PDI*(0.5*(loopFLL.acc(k)+...
                             loopFLL.acc(k-n))+a*om_0^2*loopFLL.dsc_out(k))...
                            +loopFLL.acc2(k);
        loopFLL.acc3(index)= loopPLL.PDI*(0.5*(loopFLL.acc2(k)+...
                             loopFLL.acc2(k-n))+b*om_0*loopFLL.dsc_out(k))...
                            +loopFLL.acc3(k-n);
        loopFLL.filt_out(index) = 0.5*(loopFLL.acc3(k)+loopFLL.acc3(k-n));
    end

    %% DLL Filter
    if settings.filt_ord.DLL == 1
        om_0 = settings.st_ord.DLL_om0;
        loopDLL.filt_out(index) = om_0*loopDLL.dsc_out(k);
    elseif settings.filt_ord.DLL == 2
        om_0 = settings.nd_ord.DLL_om0;
        a = settings.nd_ord.DLL_a;         
        loopDLL.acc(index)      = loopPLL.PDI*(om_0)^2*loopDLL.dsc_out(k) + loopDLL.acc(k-n);
        loopDLL.filt_out(index) = om_0*a*loopDLL.dsc_out(k) + 0.5*(loopDLL.acc(k) + loopDLL.acc(k-n));
    elseif settings.filt_ord.DLL == 3
        om_0 = settings.rd_ord.DLL_om0;
        a = settings.rd_ord.DLL_a;
        b = settings.rd_ord.DLL_b;
        loopDLL.acc(index) = loopPLL.PDI*(om_0)^3*loopDLL.dsc_out(k)+loopDLL.acc(k-n);
        loopDLL.acc2(index) = loopPLL.PDI*((om_0)^2*a*loopDLL.dsc_out(k)+loopDLL.acc(k))...
                         +loopDLL.acc2(k-n);
        loopDLL.filt_out(index) = om_0*b*loopDLL.dsc_out(k) + loopDLL.acc2(k); 
    end
    
    %% FLL-assissted-PLL
    if settings.filt_ord.FLL_PLL == 12
        om_0f = settings.st_ord.FLL_om0;
        om_0p = settings.nd_ord.PLL_om0;
        a     = settings.nd_ord.PLL_a;
        loopPLL.acc(index) = loopPLL.PDI*om_0f*loopFLL.dsc_out(k)+...
                             loopPLL.PDI*om_0p^2*loopPLL.dsc_out(k)+loopPLL.acc(k-n);
        loopPLL.filt_out(index) = a*om_0p*loopPLL.dsc_out(k)+0.5*(loopPLL.acc(k)+loopPLL.acc(k-n));
    elseif settings.filt_ord.FLL_PLL == 23
        om_0f = settings.nd_ord.FLL_om0;
        om_0p = settings.rd_ord.PLL_om0;
        af    = settings.nd_ord.FLL_a;
        ap    = settings.rd_ord.PLL_a;
        b     = settings.rd_ord.PLL_b;
        loopPLL.acc(index) = loopPLL.PDI*om_0f^2*loopFLL.dsc_out(k)+...
                             loopPLL.PDI*om_0p^3*loopPLL.dsc_out(k)+loopPLL.acc(k-n);
        loopPLL.acc2(index)= loopPLL.PDI*(0.5*(loopPLL.acc(k)+loopPLL.acc(k-n))+...
            af*om_0f*loopFLL.dsc_out(k)+ap*om_0p^2*loopPLL.dsc_out(k))+loopPLL.acc2(k-n);  
        loopPLL.filt_out(index) = 0.5*(loopPLL.acc2(k)+loopPLL.acc2(k-n))+b*om_0p*loopPLL.dsc_out(k);
    end
end