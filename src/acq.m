function [resultsACQ] = acq(signal, prn, settings)
%%              Signal acquisition
% Input:
%
%   signal     - received GNSS signal 
%   prn        - prn corresponding to satellite specified in settings
%   settings   - user defined settings (sampling freq., doppler freq.,
%                phase shift, ...)
%
% Output:
%
%   resultsACQ - results of signal acquisition
%       
%==========================================================================

%% pre-alloc
resultsACQ.samples_period = 0;
resultsACQ.fd_noncoh = 0;
resultsACQ.tau_noncoh = 0;

if (strcmp(settings.constellation, "galileo"))
    ms = 0.004;
    code_len = length(prn);
elseif (strcmp(settings.constellation, "gps L5"))
    ms = 0.001;
    code_len = length(prn);
elseif (strcmp(settings.constellation, "gps L1"))
    ms = 0.001;
    code_len = length(prn); 
else error('Constellation not supported, aborting!')    
end

w = waitbar(0, 'Acquisition...');
pause(1)
%% re-sampling
k = (settings.f_sampling)*ms;

resultsACQ.replica = zeros(1, k);
if (strcmp(settings.constellation, "galileo"))
    for i = 0:k-1    
        resultsACQ.replica(i+1) = prn(mod(fix(i*(settings.T_sampling)/(settings.T_chipn)),code_len)+1);
    end
elseif (strcmp(settings.constellation, "gps L5"))
    for i = 0:k-1    
        resultsACQ.replica(i+1) = prn(mod(fix(i*(settings.T_sampling)/(settings.T_chipn)),code_len)+1);
    end
else
    for i = 0:k-1    
        resultsACQ.replica(i+1) = prn(mod(fix(i*(settings.T_sampling)/(settings.T_chipn)),code_len)+1);
    end
end

%% segmentation 
resultsACQ.samples_period = (settings.f_sampling)*ms;
segment = zeros(resultsACQ.samples_period, settings.acq_len);
for k = 1:settings.acq_len
    segment(:,k)=signal((resultsACQ.samples_period*(k-1)+1):(resultsACQ.samples_period*(k)));
end

resultsACQ.segment = segment;

%% remove Doppler
n = length(signal);
t = linspace(0, n, n);
t = t.*(settings.T_sampling);

pause(1)
waitbar(.3, w)
%% serial
if (strcmp(settings.CAF, "serial"))
    corr_noncoh = zeros((2*length(segment))-1,(abs((settings.dop_max)-(settings.dop_min))/(settings.dop_step))+1);
    for fd = (settings.dop_min):(settings.dop_step):(settings.dop_max)  
        ex = exp(-1j*2*pi*((settings.f_0)+fd)*t);
        for k = 1:settings.acq_len
            sig = (segment(:,k)).*ex(((resultsACQ.samples_period*(k-1))+1):(resultsACQ.samples_period*k))';                 
            m = ((fd+(settings.dop_max))/(settings.dop_step))+1;
            
            % non-coherent
            corr_noncoh(:,m)=corr_noncoh(:,m)+abs(xcorr(resultsACQ.replica',sig)).^2; 
        end
        pct = (30+(fd-settings.dop_min)*(60/(settings.dop_max-settings.dop_min)))/100;
        waitbar(pct, w)
    end
    
    % serial
    [resultsACQ.Max, b]=max(max(corr_noncoh));
    resultsACQ.fd_noncoh = (b-((settings.dop_max)/(settings.dop_step))-1)*(settings.dop_step);
    [~, d]=max(max(corr_noncoh'));
    resultsACQ.tau_noncoh = (d-((length(corr_noncoh)+1)/2))*settings.T_sampling;
    [~, ax]=xcorr(resultsACQ.replica, segment(:,1));
end

%% parallel
if (strcmp(settings.CAF, "parallel"))  
    corrP_noncoh = zeros(length(segment), (abs((settings.dop_max)-(settings.dop_min))/(settings.dop_step))+1);

    for fd = (settings.dop_min):(settings.dop_step):(settings.dop_max)
        ex = exp(-2*pi*1j*((settings.f_0)+fd).*t);
        for k = 1:settings.acq_len
            sig = (segment(:,k)).*ex(((resultsACQ.samples_period*(k-1))+1):(resultsACQ.samples_period*k))'; 
            m = ((fd+(settings.dop_max))/(settings.dop_step))+1;

            % non-coherrent 
            corrP_noncoh(:,m) = corrP_noncoh(:,m) + abs(ifft(fft(sig').*conj(fft(resultsACQ.replica)))).^2';

        end
        pct = (20+(fd-settings.dop_min)*(70/(settings.dop_max-settings.dop_min)))/100;
        waitbar(pct, w)
    end
    
    waitbar(.9, w)
    pause(1)
    %% Calculate fd and tau from caf vector
    % parallel
    [resultsACQ.Max, b]=max(max(corrP_noncoh));
    resultsACQ.fd_noncoh = (b-((settings.dop_max)/(settings.dop_step))-1)*(settings.dop_step);
    [~, d]=max(max(corrP_noncoh'));
    resultsACQ.tau_noncoh = (((length(corrP_noncoh)+1))-d)*(settings.T_sampling);
    fd = (settings.dop_min):(settings.dop_step):(settings.dop_max);
    z = (0:1:length(corrP_noncoh(:,1))-1)*(settings.T_sampling);
end 
    waitbar(1, w)
    pause(1)
    close(w)

    disp(['Acquired frequency: ', num2str(resultsACQ.fd_noncoh), ' Hz'])
%%===================================================================
%% serial plot
if (strcmp(settings.CAF, "serial"))   
    x = ax.*settings.T_sampling;                      
    b = (settings.dop_min):(settings.dop_step):(settings.dop_max);
    f = figure('visible', settings.plot.acq);
    mesh(b,x,corr_noncoh);
    %axis('square');
    title('Correlation function - non-coherent', ...
      ['Doppler shift: f_d = ' num2str(resultsACQ.fd_noncoh) 'Hz, offset: tau = ' num2str(resultsACQ.tau_noncoh*1000) 'ms']);
    ylabel('time[s]');
    xlabel('f_d [Hz]');
    zlabel('E');
    saveas(f, '../figures/serialACQ.png');

    PNR_s_noncoh = max(corr_noncoh(:))/(1/(numel(corr_noncoh))*sum(corr_noncoh, 'all'));
end

%% parallel plot
if (strcmp(settings.CAF, "parallel"))
    f = figure('visible', settings.plot.acq);
    mesh(fd,z,corrP_noncoh);
    %axis('square');
    title('Correlation function using FFT - non-coherent', ...
        ['Doppler shift: f_d = ' num2str(resultsACQ.fd_noncoh) 'Hz, offset: tau = ' num2str(resultsACQ.tau_noncoh*1000) 'ms']);
    ylabel('time [s]');
    xlabel('fd [Hz]');
    zlabel('E');
    saveas(f, '../figures/parallelACQ.png');

    PNR_p_noncoh = 10*log10(max(corrP_noncoh(:))/(1/(numel(corrP_noncoh))*sum(corrP_noncoh, 'all')));
end
end