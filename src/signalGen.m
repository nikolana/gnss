function [resultsGEN, ms] = signalGen(settings, d_nav)
%%           Simulates GNSS signal
% Supported signals: GPS L1 C/A, GPS L5I, Galileo E1C  
%
% Input:
%
%    settings   - user defined settings (sampling freq., doppler freq.,
%                   phase shift, ...)
%    d_nav       - navigational message
%
% Output:
%
%    resultsGEN - structure containing generated signal, prn, ... 
%    ms         - code period in ms
%
%%=========================================================================
b = waitbar(0, 'Generating signal...');
pause(1)

if strcmp(settings.constellation, 'gps L1')
    code_len = 1023;
    ms       = 1e-3;
    % C/A code
    load 'codes_L1CA.mat';
    prn = codes_L1CA(:, settings.sat_num)'; % already +-1
    resultsGEN.prn = prn;
    if settings.sig_ps > settings.f_sampling*0.001
        len_sec = (settings.sig_len+(settings.sig_ps/(settings.f_sampling*0.001)))*0.001; % calculate no. of additional periods
    else
        len_sec = (settings.sig_len+1)*0.001;
    end
elseif strcmp(settings.constellation, 'gps L5')
    code_len = 10230;
    ms       = 1e-3;
    % L5I code
    load 'codes_L5I.mat';
    prn = codes_L5I(:, settings.sat_num)'; % already +-1
    resultsGEN.prn = prn;
    if settings.sig_ps > settings.f_sampling*ms
        len_sec = (settings.sig_len+(settings.sig_ps/(settings.f_sampling*ms)))*ms; % calculate no. of additional periods
    else
        len_sec = (settings.sig_len+1)*ms;
    end
elseif strcmp(settings.constellation, 'galileo')
    ms       = 4e-3;
    f_sub    = 1.023e6;
    % E1C code
    load 'codes_E1C.mat';
    prn = codes_E1C(:, settings.sat_num)'; % already +-1
    prn_rtz = zeros(1, 2*length(prn));
    code_len = length(prn_rtz);
    prn = rtz(prn, prn_rtz);
    resultsGEN.prn = prn;
    if settings.sig_ps > settings.f_sampling*ms
        len_sec = (settings.sig_len+(settings.sig_ps/(settings.f_sampling*ms)))*ms; % calculate no. of additional periods
    else
        len_sec = (settings.sig_len+1)*ms;
    end
else error('Constellation not supported, aborting!')    
end

%% modulation
D = settings.upsampling_ratio;
fs = D*settings.f_sampling;
Ts = 1/fs;
k = floor(fs*len_sec);
replica = zeros(1, k);
signal = zeros(1, k);
t = 0:floor(k-1); % time vector
t = t.*Ts;
rate_ps = settings.f_rate*Ts; % Doppler rate per sample
f_d = settings.f_doppler;
on  = settings.jerk;

c = 299792458;
v = zeros(1, floor(fs*len_sec));
%% re-sampling
for i = 1:(fs*len_sec)    
    %% update doppler shift
    f_d = f_d + 0.5*rate_ps*i^(2*on);
    f_chip = settings.f_chipn + (f_d*(settings.f_chipn/settings.f_carrier)) * settings.code_on; % [Hz]
    T_chip = 1/f_chip; % [s]
    arg = mod(fix((i)*Ts/T_chip),code_len)+1;
    arg_nav = mod(floor((fix((i)*Ts/T_chip))/(20*code_len)), code_len)+1;
    
    replica(i) = d_nav(arg_nav).*prn(arg);
    %% carrier
    w = 2*pi*(settings.f_0+f_d);
    if settings.carrier_on
        carrier = exp(-1j*w.*t(i) + 1j*settings.carrier_bias);
    else carrier = 1;
    end
    
    if ~ strcmp(settings.constellation, 'galileo')
        signal(i) = replica(i).*carrier;
    else
        signal(i) = replica(i).*carrier; %.*sign(cos(2*pi*f_sub.*t(i)));
    end
    
    if mod(i, 1000) == 0
        pct = i/(fs*len_sec);
        waitbar(pct, b)
    end
    
    v(i) = c*(f_d)/settings.f_carrier; % calculate mutual velocity
end

a = round(diff(v)./diff(t), 4);
resultsGEN.RxTx_veloc = decimate(v(1:(fs*settings.sig_len*0.001)), settings.f_sampling*1e-3);
resultsGEN.RxTx_accel = decimate(a(1:(fs*settings.sig_len*0.001)), settings.f_sampling*1e-3);

%% Additive White Gaussian Noise
if settings.noise_on
    P = 1/length(replica)*sum(replica.*conj(replica)); % power of replica signal
    SNR_dB = settings.cn0-10*log10(fs); % C_N0 is typically [37, 45] for GPS L1
    SNR = 10^(SNR_dB/10);
    N0 = P/SNR; % linear scale ; power of white noise
    sigma = sqrt(N0/2);
    noise = sigma*(randn(1,length(replica))+1j*randn(1,length(replica))); % noise vector
    signal = signal+noise;

    N0_gen = (1/length(noise)*sum(noise.*conj(noise)));
    SNR_gen = P / N0_gen;
    C_N0_dB = 10*log10(SNR_gen)+10*log10(fs);
    
    disp(['Generated signal C/N0: ', num2str(C_N0_dB), ' dB-Hz'])
    disp(['Generated signal SNR: ', num2str(10*log10(SNR_gen)), ' dB'])
end

%% code phase shift
N = settings.sig_len*0.001;
signal = signal((D*settings.sig_ps+1):(D*settings.sig_ps+N*fs));

%% decimation
waitbar(90, b);
pause(1)
signal_d = zeros(1, ceil(length(signal)/D)); % decimated signal
k = 1;
for i = 1:D:length(signal)  
    signal_d(k) = (1/D) * sum(signal(i:(i+D-1)));
    k = k+1;
end
waitbar(100, b);
close(b);
resultsGEN.signal = signal_d; 
%% plot
f = figure('visible', settings.plot.signal);
subplot(3, 1, 1);
plot(t(1:(ms*fs)), replica(1:(ms*fs)));
title('C/A Code', ['Satellite ' num2str(settings.sat_num)])
axis([0 ms -1.5 1.5]);
xlabel('time [s]')

t=linspace(0,(settings.f_sampling)*N,(settings.f_sampling)*N); % time vector
t=t.*(settings.T_sampling);

subplot(3, 1, 2);
plot(t((1:(ms*fs))), real(signal_d(1:(ms*fs))));
title('Modulated carrier - Inphase')
axis([0 ms -1.5 1.5]);
xlabel('time [s]')

subplot(3, 1, 3);
plot(t((1:(ms*fs))), imag(signal_d((1:(ms*fs)))));
title('Modulated carrier - Quadrature')
axis([0 ms -1.5 1.5]);
xlabel('time [s]')
saveas(f, '../figures/signal.fig');
end
