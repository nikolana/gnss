%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
%%                   GNSS Software Defined Radio                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% author: Anastas Nikolov, FEE CTU in Prague,                             %
%                          Department of Radiotechnology                  %
% year: 2021                                                              %
% contact: anastas.nikolov@fel.cvut.cz                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all, clear all, clc     

%%                     User defined settings
%==========================================================================
%% constellation
settings.CAF           = 'serial'; % [serial / parallel]
settings.constellation = 'galileo'; % [gps L1 / galileo / gps L5]
settings.sat_num       = 13;

%% signal length
settings.sig_len = 3000; % [ms]
settings.acq_len = 10; % [code period]

%% signal properties
settings.f_chipn   = 2*1.023e6;   % [Hz] ; nominal chip f
                                % set to double if using Galileo
settings.f_carrier = 1575.42e6; % [Hz] %1575.42e6; GPS L1 + Galileo E1C
                                %1176.45e6; GPS L5 
settings.T_chipn   = 1/(settings.f_chipn); % [s]

%% ADC properties
settings.f_sampling       = 2.5e6; % [Hz]
settings.T_sampling       = 1/(settings.f_sampling); % [s]
settings.f_0              = 0; % [Hz] ; intermediate frequency
settings.upsampling_ratio = 1; % [-]    

%% dynamic stress
settings.f_doppler    = 1250; % [Hz] starting Doppler effect
settings.f_rate       = 0.05; % [Hz/s] rate of change of Doppler effect
                           % [Hz/s^2] if jerk is set on 
settings.jerk         = 0; % 1 -> jerk on ; does not work very well
settings.sig_ps       = 200; % [samples] ; code phase shift
settings.carrier_bias = pi/4; % phase bias of channel 
settings.code_bias    = 2; % code phase bias (to kick-start DLL)

%% acquisition
settings.dop_max  = 6000; % [Hz]
settings.dop_min  = -(settings.dop_max); % [Hz]
settings.dop_step = 100; % [Hz]

%% noise properties
settings.noise_on = 1; % 0 -> noise off
settings.cn0      = 50; % [dB-Hz]

%% tracking
settings.corr_wdth = 0.25; % correlator width [chip]

    % discriminators
    settings.dsc.DLL = 'e-l';   % ['e-l', 'e-l_pow', 'quasi', 'dot']
    settings.dsc.PLL = 'atan';  % ['cross', 'sign', 'Q/I', 'atan', 'atan2'] 
    settings.dsc.FLL = 'atan';  % ['cross', 'sign', 'atan', 'atan2']
    
    % noise bandwidth
    settings.DLL.Bn = 2; % [Hz]
    settings.PLL.Bn = 15; % [Hz]
    settings.FLL.Bn = 15; % [Hz]

    % filter order
    settings.filt_ord.DLL     = 2; % [0, 1, 2, 3]
    settings.filt_ord.PLL     = 0; % [0, 1, 2, 3]
    settings.filt_ord.FLL     = 2; % [0, 1, 2]
    settings.filt_ord.FLL_PLL = 0; % [0, 12, 23] ; FLL-assisted-PLL
                                   % 12 -> 1st ord. FLL, 2nd ord. PLL

    % filter coefficients
    settings.st_ord.DLL_om0 = 4*settings.DLL.Bn;
    settings.nd_ord.DLL_om0 = settings.DLL.Bn/0.53;
    settings.nd_ord.DLL_a = sqrt(2);
    settings.rd_ord.DLL_om0 = settings.DLL.Bn/0.7845;
    settings.rd_ord.DLL_a = 1.1;
    settings.rd_ord.DLL_b = 2.4;
    
    settings.st_ord.FLL_om0 = 4*settings.FLL.Bn;
    settings.nd_ord.FLL_om0 = settings.FLL.Bn/0.53;
    settings.nd_ord.FLL_a = sqrt(2);
    settings.rd_ord.FLL_om0 = settings.PLL.Bn/0.7845;
    settings.rd_ord.FLL_a = 1.1;
    settings.rd_ord.FLL_b = 2.4;
    
    settings.st_ord.PLL_om0 = 4*settings.PLL.Bn;
    settings.nd_ord.PLL_om0 = settings.PLL.Bn/0.53;
    settings.nd_ord.PLL_a = sqrt(2);  
    settings.rd_ord.PLL_om0 = settings.PLL.Bn/0.7845;
    settings.rd_ord.PLL_a = 1.1;
    settings.rd_ord.PLL_b = 2.4;

%% debug options
settings.carrier_on = 1; % 0 -> carrier wiped off
settings.code_on    = 1; % 0 -> code is locked
settings.DLL_fb     = 1; % 0 -> DLL feedback off
settings.PLL_fb     = 0; % 0 -> PLL feedback off  (set 1 when using FLL_PLL)
settings.FLL_fb     = 1; % 0 -> FLL feedback off

%% graphical settings
% figures are saved to ../figures/ even with 'off' option
% they can be opened with >> openfig('path', 'visible')
settings.plot.signal = 'off';   % plot generated signal [on / off]
settings.plot.acq    = 'off';   % plot results of acquisition [on / off]
settings.plot.DLL    = 'on';   % plot DLL output [on / off]
settings.plot.PLL    = 'on';   % plot PLL output [on / off]
settings.plot.FLL    = 'on';   % plot FLL output [on / off]
settings.plot.cor    = 'on';   % plot correlators [on / off]
settings.plot.filt   = 'on';   % plot integrators and filter output [on / off]
settings.plot.veloc  = 'on';   % plot mutual velocity estimate [on / off]

%%========================================================================================

disp('          GNSS Software Defined Radio          ')                                 
disp(['Constellation: ', settings.constellation])
disp(['Acquisition algorithm: ', num2str(settings.CAF)])
disp(['Satellite number: ', num2str(settings.sat_num)])
disp(['ADC sampling frequency: ', num2str(settings.f_sampling/1e6), ' MHz'])
disp(['Intermediate frequency: ', num2str(settings.f_0), ' Hz'])
disp(['C/N0: ', num2str(settings.cn0), ' dB-Hz'])
disp(['Signal length: ', num2str(settings.sig_len), ' ms'])

%%========================================================================================

%%                      Main program
%%========================================================================================

%% generate GNSS signal
n = 2*ceil(settings.sig_len/20); % n/2 [1,-1] blocks in d_nav needed
d_nav = ones(1, n);

% uncomment this to generate random navigational data (20 ms bit period)
%    for k = 1:n
%        d_nav(k) = 2*randi([0,1]) - 1;
%    end

[resultsGEN, ms] = signalGen(settings, d_nav);
% to process real signal, comment line above and choose R-S or real signal

% Rhode-Schwarz
% load('/home/taso/ownCloud/Shared/NIKOLOV_BP_2022/SDR_records/3050measure_TEST_2022-05-06-12-47-26.mat');

% real signal
% load('../live_signal/3050measure_TEST_2022-05-10-14-33-28.mat'); 

% uncomment this when testing real (or R-S) signal
% load codes_E1C.mat;
% resultsGEN.signal = RXDATA'; 
% prn = codes_E1C(:, settings.sat_num)';
% prn_rtz = zeros(1, 2*length(prn));
% resultsGEN.prn = rtz(prn, prn_rtz);
% resultsGEN.RxTx_veloc = zeros(1, settings.sig_len);
% ms = 4e-3;

%% call acquisition script
resultsACQ = acq(resultsGEN.signal(1:(settings.acq_len*settings.f_sampling*ms)), ...
                  resultsGEN.prn, settings);   
 
%% call tracking script
resultsTCK = tracking(settings, resultsGEN, resultsACQ);
