function [prn] = rtz(prn, prn_rtz)
%%           Return To Zero
% Generates modified PRN for BOC modulations 
%
% Input:
%
%    prn     - original pseudo-random noise
%    prn_rtz - pre-allocated vector for modified PRN
%
% Output:
%
%    prn     - modified return-to-zero PRN vector
%
%%=========================================================================
    ind = 1;
    for n = 1:length(prn)
        prn_rtz(ind)   = prn(n);
        prn_rtz(ind+1) = -prn(n);
        ind = ind+2;
    end
    prn = prn_rtz;
end