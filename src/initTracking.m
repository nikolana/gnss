function [loopDLL, loopPLL, loopFLL, codeNCO] = initTracking(settings, resultsACQ)
    %%              Initialize tracking loop structures
    % Input:
    %
    %   settings   - user defined settings (sampling freq., doppler freq.,
    %                phase shift, ...)
    %   resultsACQ - results of signal acquisition 
    %
    % Output:
    %
    %   loopDLL    - Delay locked loop structure
    %   loopPLL    - Phase locked loop strucrure
    %   loopFLL    - Frequency locked loop structure
    %   codeNCO    - Code NCO structure
    %   debug      - Used to verify tracking results
    %
    %%=========================================================================
                        %%%% Code NCO %%%
    codeNCO.fc       = settings.f_chipn;
    codeNCO.fs       = settings.f_sampling;
    codeNCO.Ts       = settings.T_sampling;                    
    
                        %%%% DLL %%%%
    loopDLL.peak_fd = ((resultsACQ.fd_noncoh-(settings.dop_min))/(settings.dop_step))+1; 
                                                        % peak coordinate on [Hz] axis
    
    loopDLL.P = round(resultsACQ.tau_noncoh*settings.f_sampling)-1 + settings.code_bias;
    
    disp(['Prompt at ', num2str(loopDLL.P)])
    disp(['Coordinates of CAF peak [tau, f_d] = [', num2str(loopDLL.P),...
          ',', num2str(loopDLL.peak_fd), ']'])                      
    
    loopDLL.epl      = zeros(settings.sig_len, 3);  % E, P, L vector
    loopDLL.dsc_out  = zeros(settings.sig_len, 1);  % discriminator-out vector
    loopDLL.filt_out = zeros(settings.sig_len, 1);  % filter-out vector
    loopDLL.Ip       = zeros(settings.sig_len, 1);  % prompt in-phase
    loopDLL.Qp       = zeros(settings.sig_len, 1);  % prompt quadrature
    loopDLL.Ie       = zeros(settings.sig_len, 1);  % early in-phase
    loopDLL.Qe       = zeros(settings.sig_len, 1);  % early quadrature
    loopDLL.Il       = zeros(settings.sig_len, 1);  % late in-phase
    loopDLL.Ql       = zeros(settings.sig_len, 1);  % late quadrature
    loopDLL.acc      = zeros(settings.sig_len, 1);  % accumulator
    loopDLL.acc2     = zeros(settings.sig_len, 1);  % accumulator 2
    
                        %%%% PLL %%%%
    % Predetection Integration Time - 1 code period by default 
    if strcmp(settings.constellation, 'galileo') 
        loopPLL.PDI = 4e-3;
    elseif strcmp(settings.constellation, 'gps L5')
        loopPLL.PDI = 1e-2;
    else
        loopPLL.PDI = 1e-3;
    end
    loopPLL.I_ps1    = 0;
    loopPLL.I_ps2    = 0;
    loopPLL.Q_ps1    = 0;
    loopPLL.Q_ps2    = 0; 
    loopPLL.filt_out = zeros(settings.sig_len, 1);
    loopPLL.acc      = zeros(settings.sig_len, 1);
    loopPLL.dsc_out  = zeros(settings.sig_len, 1);
    loopPLL.acc2     = zeros(settings.sig_len, 1);  
    
                        %%%% FLL %%%% 
    loopFLL.dot      = zeros(settings.sig_len, 1);
    loopFLL.cross    = zeros(settings.sig_len, 1);
    loopFLL.filt_out = zeros(settings.sig_len, 1);
    loopFLL.acc      = zeros(settings.sig_len, 1);
    loopFLL.acc2     = zeros(settings.sig_len, 1);
    loopFLL.acc3     = zeros(settings.sig_len, 1);
    loopFLL.dsc_out  = zeros(settings.sig_len, 1); 
end