# GNSS Software Defined Radio

Custom signal generation and detailed modeling of tracking loops

## Description

This project is developed in context of bachelor's thesis at CTU in Prague,
department of Radioelectronics. 

## Getting Started

### Dependencies

Only Matlab is needed to run this program (recommended R2021a).

### Installing

* Clone this repository or download it as .zip
* Make sure _figures/_ folder exists in _gnss/_

### Executing program

* Adjust settings in _sdr.m_ to fit your satellite and signal
* Set up the tracking loops
```
% filter order
settings.filt_ord.DLL     = 1; % [0, 1, 2, 3]
settings.filt_ord.PLL     = 0; % [0, 1, 2, 3]
settings.filt_ord.FLL     = 2; % [0, 1, 2]
settings.filt_ord.FLL_PLL = 0; % [0, 12, 23] ; FLL-assisted-PLL
                               % 12 -> 1st ord. FLL, 2nd ord. PLL
```
* Remember to turn the correct feedbacks on
```
settings.carrier_on = 1; % 0 -> carrier wiped off
settings.code_on    = 1; % 0 -> code is locked
settings.DLL_fb     = 1; % 0 -> DLL feedback off
settings.PLL_fb     = 0; % 0 -> PLL feedback off  (set 1 when using FLL_PLL)
settings.FLL_fb     = 1; % 0 -> FLL feedback off
```
* Choose which figures interst you
```
settings.plot.signal = 'off';   % plot generated signal [on / off]
settings.plot.acq    = 'off';   % plot results of acquisition [on / off]
settings.plot.DLL    = 'on';   % plot DLL output [on / off]
settings.plot.PLL    = 'on';   % plot PLL output [on / off]
settings.plot.FLL    = 'on';   % plot FLL output [on / off]
settings.plot.cor    = 'on';   % plot correlators [on / off]
settings.plot.filt   = 'on';   % plot integrators and filter output [on / off]
settings.plot.veloc  = 'on';   % plot mutual velocity estimate [on / off]
```
* Run the program from Matlab editor or from Matlab console as
```
>> sdr
```

## Authors

Anastas Nikolov <br />
nikolana@fel.cvut.cz

## Acknowledgments

Borre		 - A Software-Defined GPS and Galileo Receiver <br />
Kaplan & Hegarty - Understanding GPS GNSS. Principles and Applications (2018, Artech)





